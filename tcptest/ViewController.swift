//
//  ViewController.swift
//  tcptest
//
//  Created by KawakawaPlanning on 2015/06/03.
//  Copyright (c) 2015年 KawakawaPlanning. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate , UISearchBarDelegate {

    var Connection1 = Connection()
    var isInLoad = false
    var list: [String] = [""]
    var ids: [String] = [""]
    static var getIp : CFString = "192.168.XXX.XXX"
    static var getPort : UInt32 = 10000
    
    let defaults = NSUserDefaults.standardUserDefaults()
    
    @IBOutlet weak var table: UITableView!
    
    @IBOutlet weak var searchBar: UISearchBar!
    
    // セルの行数
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return list.count
    }
    
    // セルの内容を変更
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell: UITableViewCell = UITableViewCell(style: UITableViewCellStyle.Subtitle, reuseIdentifier: "Cell")
        
        cell.textLabel?.text = list[indexPath.row]
        return cell
    }
    
    // セル選択リスナ
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        println(list[indexPath.row] + ids[indexPath.row])
        Connection1.sendCommand(ids[indexPath.row] + "," + list[indexPath.row])
    }
    
    @IBAction func getAsync() {
        SVProgressHUD.showWithStatus("検索中")
        self.list.removeAll(keepCapacity: false)
        self.ids.removeAll(keepCapacity: false)
        self.table.reloadData()
        // 通信先のURLを生成
        var searchUrl = searchBar.text.stringByAddingPercentEscapesUsingEncoding(NSUTF8StringEncoding)!
        var myUrl:NSURL = NSURL(string:"https://www.googleapis.com/youtube/v3/search?key=AIzaSyBEdFSE1PClEDQ2AnvQJ-SGe5QM9VIXJBQ&part=id,snippet&maxResults=50&type=video&q=" + searchUrl)!
        // リクエストを生成
        var task = NSURLSession.sharedSession().dataTaskWithURL(myUrl, completionHandler: {data, response, error in
            // リソースの取得が終わると、ここに書いた処理が実行される
            println(response)
            var json = JSON(data: data)
            for var i = 0; i < 50; i++ {
                var id = json["items"][i]["id"]["videoId"]
                var title = json["items"][i]["snippet"]["title"]
                print(title)
                print(",")
                println(id)
                self.list.append("\(title)")
                self.ids.append("\(id)")
                
            }
            println("reroadstart")
            self.table.reloadData()
            println("reroadfinishd")
            // ロードが完了したので、falseに
            self.isInLoad = false
            SVProgressHUD.dismiss()
        })
        task.resume()
        
        
        var myRequest:NSURLRequest  = NSURLRequest(URL: myUrl)
        // 送信処理を始める.
        NSURLConnection.sendAsynchronousRequest(myRequest, queue: NSOperationQueue.mainQueue(), completionHandler: self.getHttp)
    }
    func getHttp(res:NSURLResponse?,data:NSData?,error:NSError?){
        
        var myData:NSString = NSString(data: data!, encoding: NSUTF8StringEncoding)!
        
    }
    
    
    /*
    Searchボタンが押された時に呼ばれる
    */
    func searchBarSearchButtonClicked(searchBar: UISearchBar) {
        self.view.endEditing(true)
        getAsync()
        
        var getIpOb: AnyObject? = defaults.objectForKey("ip")
        
        ViewController.getIp = defaults.stringForKey("ip")!
        ViewController.getPort = UInt32(defaults.integerForKey("port"))
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        table.delegate = self
        table.dataSource = self
        searchBar.delegate = self
        
        // process if this is first time to launch this app

        if defaults.boolForKey("firstLaunch") {
            
            defaults.setBool(false, forKey: "firstLaunch")
            println("first")
            
            defaults.setObject("192.168.XXX.XXX", forKey: "ip")
            defaults.setInteger(10000, forKey: "port")
        }
        var getIpOb: AnyObject? = defaults.objectForKey("ip")
        
        ViewController.getIp = "\(getIpOb)"
        ViewController.getPort = UInt32(defaults.integerForKey("port"))
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

class Connection :NSObject, NSStreamDelegate {
    
    let serverAddress: CFString = ViewController.getIp
    let serverPort: UInt32 = ViewController.getPort
    
    private var inputStream: NSInputStream!
    private var outputStream: NSOutputStream!
    
    func connect(){

    }
    
    func stream(aStream: NSStream, handleEvent eventCode: NSStreamEvent) {
        //println(stream(
    }
    
    func sendCommand(command: String){
        
        println("connecting... \(serverAddress) \(serverPort)")
        
        var readStream:  Unmanaged<CFReadStream>?
        var writeStream:  Unmanaged<CFWriteStream>?
        
        CFStreamCreatePairWithSocketToHost(nil, self.serverAddress, self.serverPort,&readStream,&writeStream)
        
        self.inputStream = readStream!.takeRetainedValue()
        self.outputStream = writeStream!.takeRetainedValue()
        
        self.inputStream.delegate = self
        self.outputStream.delegate = self
        
        self.inputStream.scheduleInRunLoop(NSRunLoop.currentRunLoop(), forMode: NSDefaultRunLoopMode)
        self.outputStream.scheduleInRunLoop(NSRunLoop.currentRunLoop(), forMode: NSDefaultRunLoopMode)
        
        self.inputStream.open()
        self.outputStream.open()
        
        println("connect siccess!!")
        
        var ccommand = command.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: false)!
        self.outputStream.write(UnsafePointer(ccommand.bytes), maxLength: ccommand.length)
        println("Send: \(command)")
        
        self.outputStream.close()
        self.outputStream.removeFromRunLoop(NSRunLoop.currentRunLoop(), forMode: NSDefaultRunLoopMode)
        self.outputStream = nil
        
        self.inputStream.close()
        self.inputStream.removeFromRunLoop(NSRunLoop.currentRunLoop(), forMode: NSDefaultRunLoopMode)
        
        self.inputStream = nil
    }
}