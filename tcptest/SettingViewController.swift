//
//  SettingViewController.swift
//  
//
//  Created by KawakawaPlanning on 2015/06/04.
//
//

import UIKit

class SettingViewController: UIViewController {

    @IBOutlet weak var ipTxt: UITextField!
    @IBOutlet weak var portTxt: UITextField!
    
    let defaults = NSUserDefaults.standardUserDefaults()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        var getIp = defaults.stringForKey("ip")
        var getPort = defaults.integerForKey("port")
        
        ipTxt.text = getIp
        portTxt.text = "\(getPort)"
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func saveBtnOnClick(){
        defaults.setObject(ipTxt.text, forKey: "ip")
        defaults.setInteger(portTxt.text.toInt()!, forKey: "port")
    }
    
}
